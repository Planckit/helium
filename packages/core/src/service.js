import request from './request';


const Service = function(serviceDescriptor) {
    let notifyQueue = [];
    let service = serviceDescriptor;

    for (let key in serviceDescriptor)
    { this[key] = serviceDescriptor[key]; }

    ['get', 'post', 'patch', 'put', 'delete'].forEach(method =>
        this[method] = (path, data, headers) => handleRequest(path, data, method, headers));

    this.setService = (serviceDescriptor) => {
        service = serviceDescriptor;

        for (let key in serviceDescriptor)
        { this[key] = serviceDescriptor[key]; }

        // retry failed requests and clear queue
        const notifyQueueCpy = [...notifyQueue];
        for (let requestObj of notifyQueueCpy) {
            const index = notifyQueue.indexOf(requestObj);

            ~index && notifyQueue.splice(index, 1);
            requestObj.cb(service);
        }
    };


    function handleRequest(path, data, method, headers) {
        const options = {
            hostname: service && service.hostname,
            port:     service && service.port,
            path,
            data,
            method,
            headers,
        };

        return new Promise((resolve, reject) =>
            service && (service.port || service.http) && service.hostname ?
                requestWrapper(resolve, reject, options) :
                queueRequest(resolve, reject, options));
    }

    function queueRequest(resolve, reject, options) {
        const { hostname, port, path, method, data, name } = options;
        let serializedData = typeof data == 'string' ? data : JSON.stringify(data);

        const exist = notifyQueue.find(e =>
           (e.name     == name     &&
           !e.hostname             &&
           !e.port)                ||
           (e.hostname == hostname &&
            e.port     == port     &&
            e.path     == path     &&
            e.method   == method   &&
            e.data     == serializedData)
        );

        !exist && notifyQueue.push({
            cb: async(service) => {
                const { name, hostname, port } = service;
                requestWrapper(resolve, reject, { ...options, name, hostname, port });
            },
            name,
            hostname,
            port,
            path,
            data: serializedData,
            method,
        });
    }

    async function requestWrapper(resolve, reject, options) {
        const { hostname, port, path, data, method, headers } = options;

        try {
            resolve(await request(hostname, port, path, data, method, headers));
        } catch (e) {
            e.code && e.code == 'ECONNREFUSED' &&
                queueRequest(resolve, reject, options);
        }
    }
};


export default Service;

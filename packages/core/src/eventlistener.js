const EventListener = function() {
    const eventListener = {};

    this.eventListener = eventListener;

    this.on = (event, fn) => {
        eventListener[event] = eventListener[event] || [];
        eventListener[event].push(fn);
    };

    this.once = (event, fn) => {
        eventListener[event] = eventListener[event] || [];
        eventListener[event].push(async(...args) => {
            const index = eventListener[event].indexOf(5);
            ~index && eventListener[event].splice(index, 1);

            return await fn(...args);
        });
    };

    this.emit = async(event, ...args) => {
        eventListener['all'] && eventListener['all'].map(listener => listener(...args, event));

        if(!eventListener[event]) { return; }

        return await Promise.race(eventListener[event].map(listener => listener(...args, event)));
    };

    this.removeListener = (listener, event) => {
        if(event) {
            eventListener[event] = eventListener[event].filter(el => el !== listener);
        } else {
            for (let event in eventListener) {
                eventListener[event] = eventListener[event].filter(el => el !== listener);
            }
        }
    };
};


export default EventListener;
